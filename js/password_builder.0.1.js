/**
 * Password Builder
 *
 * @license MIT License
 * @author Dale Larsen
 *
 */
var passwords = {
	build: function(opts){
	
		var defs = {
			phrase: '',
			site: '',
			number: 22,
			mixCaps: true
		};
		
		opts = $.extend({}, defs, opts);
		
		var phrase = opts.phrase.dStrings('removePunctuation').toLowerCase();
		
		var phrase_arr = phrase.split(' ');
		
		for (var i = 0; i < phrase_arr.length; i++) {
			var shortened = phrase_arr[i].dStrings('shortenStr');
			if (shortened) {
				phrase_arr[i] = shortened;
			} else {
				phrase_arr[i] = phrase_arr[i].substring(0, 1);
				if (i.isEven() && opts.mixCaps) {
					phrase_arr[i] = phrase_arr[i].toUpperCase();
				}
			}
		}
		
		phrase = phrase_arr.join('');
		
		var site_str = opts.site.dStrings('removeVowels', true);
		
		var finalPhrase = phrase + opts.number.toString() + ':' + site_str;
		
		return finalPhrase;
		
	}
};
/****************
 * These are string methods I have written which are used in the password builder
 ****************
 */
/**
 * @param {Number} num
 * detect if the given number is even
 */
function detectEven(num){
	if (num > 0) {
		if (num % 2 == 0) {
			return true;
		} else {
			return false;
		}
	}
}

Number.prototype.isEven = function(){
	if (isNaN(this)) {
		return false;
	} else {
		return detectEven(this);
	}
};
var dStrings = {
	allVowels: /a|e|i|o|u|y/gi,
	removeVowels: function(str, keepFirst){
		var firstLetterIsVowel = str.substring(0, 1).match(this.allVowels);
		return (keepFirst && firstLetterIsVowel ? str.substring(0, 1) : '') + str.replace(this.allVowels, '');
	},
	shortenStrArr: [{
		_long: 'and',
		_short: '&'
	}, {
		_long: 'to',
		_short: '2'
	}, {
		_long: 'for',
		_short: '4'
	}, {
		_long: 'one',
		_short: '1'
	}, {
		_long: 'two',
		_short: '2'
	}, {
		_long: 'three',
		_short: '3'
	}, {
		_long: 'four',
		_short: '4'
	}, {
		_long: 'five',
		_short: '5'
	}, {
		_long: 'six',
		_short: '6'
	}, {
		_long: 'seven',
		_short: '7'
	}, {
		_long: 'eight',
		_short: '8'
	}, {
		_long: 'nine',
		_short: '9'
	}, {
		_long: 'ten',
		_short: '10'
	}],
	shortenStr: function(str){
		var wasChanged = false;
		for (var i = 0; i < this.shortenStrArr.length; i++) {
			if (str == this.shortenStrArr[i]._long) {
				str = this.shortenStrArr[i]._short;
				wasChanged = true;
			}
		}
		if (wasChanged) {
			return str;
		} else {
			return false;
		}
	},
	removePunctuationRegeXp: /\,|\.|\"|\'|\~|\`|\:|\;|\?/gi,
	removePunctuation: function(str){
		return str.replace(this.removePunctuationRegeXp);
	}
};
String.prototype.dStrings = function(_fn, arg1){
	return dStrings[_fn](this, arg1);
};
